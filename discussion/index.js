// alert("Hello World");
// console.log("Hello World!")
// console.log(3+2)

// Variables
/* 
This is a 
multi-line comment */


let name;
name = "Zuitt";
name = 'Emmanuel';
console.log(name);

let age = 22;

const boilingPoint = 100;
console.log(boilingPoint);
console.log(age + '3');
console.log(name+3);

let isAlive = true;
console.log(isAlive);

//Arrays
let grades = [98, 96, 95, 90];

let batch197 = ["marnel","jesus"];

let grade1 = 98;
let grade2 = 96;
let grade3 = 95;
let grade4 = 90;

console.log(grades[9]);

let movie = "jaws";

// console.log(song);

let isp;
console.log(isp);

//undefined - if exists in memory space but hasn't been assigned any value
// not defined - does not exist in memory space


//null vs undefined

//Undefined - declared but without value

//null - used intentionally to express the absence of a value in a declared/init variable

let spouse = null;
console.log(spouse);

//Objects - special kind of data type that is used to mimic real world objects/items

/* 
    Syntax:
        let objectName = {
            property1: keyValue1, 
            property2: keyValue2,
            property3: keyValue3
        }
*/

let myGrades = {
    firstGrading: 98,
    secondGrading: 92,
    thirdGrading: 90,
    fourthGrading:94.3
}

console.log(myGrades)

let person = {
    fullName: 'Alonzo Cruz',
    age: 25,
    isMarried: false,
    contact: ['091912346578', '091345679825'],
    address: {
        houseNumber: 345,
        City: 'Manila'
    }
}

console.log(person)



