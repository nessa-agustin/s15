let firstName = 'Dany';
console.log('First Name: ' + firstName);

let lastName = 'Agustin';
console.log('Last Name: ' + lastName);

let age = 4;
console.log('Age: ' + age);

let hobbies = ['eating', 'watching TV', 'playing games'];
console.log('My hobbies includes :');
console.log(hobbies);


let workAddress = {
    unitNumber: 117,
    street: 'Sampaguita St',
    brgy: 'Margot',
    city: 'Angeles City',
    province: 'Pampanga',
    zipCode: 2009
};
console.log('My Work Address: ');
console.log(workAddress);

let fullName = "Nessa Agustin";
console.log("My full name is" + fullName);

let currentAge = 40;
console.log("My current age is: " + currentAge);

let friends = ['Dany','Tintin','Ramboy'];
console.log("My Friends are: ")
console.log(friends);

let profile = {

    username: "nhey.agustin",
    fullName: "Nhey Agustin",
    age: 40,
    isActive: true,

}; 
console.log("My Full Profile: ")
console.log(profile);

let bff = "Erlyn";
console.log("My bestfriend is: " + bff);

const lastLocation = "my house";
console.log("You may find me at " + lastLocation);


